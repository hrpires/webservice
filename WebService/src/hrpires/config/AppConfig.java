package hrpires.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

@ApplicationPath("/rest")
public class AppConfig extends Application {
	@Override
	public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
       
        resources.add(CORSResponseFilter.class);
        resources.add(MultiPartFeature.class);
        resources.add(hrpires.rest.Upload.class);
        
        return resources;
    }
	
	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> props = new HashMap<>();
		props.put("jersey.config.server.provider.packages", "hrpires.rest");
		return props;
	}
}
