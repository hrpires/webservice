package hrpires.rest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import hrpires.service.Reconhecedor;
import hrpires.service.Retorno;

/**
*
* @author Heverton Pires da Luz
*/

@Path("/upload")
public class Upload {
	

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Retorno> postSom(@FormDataParam("file") InputStream data,
			@FormDataParam("file") FormDataContentDisposition info) throws IOException {
		File arq = null;
		try {
			SimpleDateFormat dataFormatada = new SimpleDateFormat("HHMMSS");
			String nome = dataFormatada.format(new GregorianCalendar().getTime());
			arq = new File(Reconhecedor.getDirTeste() + nome + ".wav");

			Files.copy(data, arq.toPath());
			if (arq.exists()) {
				return Reconhecedor.reconhecerNotas(arq.getPath());
			}else {
				throw new IOException("Erro ao tentar realizar o reconhecimento");
			}

		} catch (IOException ex) {
			throw new IOException("Erro no upload do arquivo");
		}

	}

	@GET
	public boolean isConexao() {
		Reconhecedor.ini();
		
		return true;
	}
}
