package hrpires.service;

import java.util.ArrayList;

/**
*
* @author Heverton Pires da Luz
*/

public class Retorno {
	private double[] arResultado;
	private ArrayList<Double> arResultEsperado;
	private String nomeNota;
	private String nomeMusical;
	private double probAcerto;
	
	public Retorno() {
		// TODO Auto-generated constructor stub
	}

	public Retorno(double[] arResultado, ArrayList<Double> arResultEsperado, String nomeNota, String nomeMusical,
			double probAcerto) {
		super();
		this.arResultado = arResultado;
		this.arResultEsperado = arResultEsperado;
		this.nomeNota = nomeNota;
		this.nomeMusical = nomeMusical;
		this.probAcerto = probAcerto;
	}



	public void setRetorno(double[] arResultado, ArrayList<Double> arResultEsperado, String nomeNota, String nomeMusical,
			double probAcerto) {
		this.arResultado = arResultado.clone();
		this.arResultEsperado = arResultEsperado;
		this.nomeNota = nomeNota;
		this.nomeMusical = nomeMusical;
		this.probAcerto = probAcerto;
	}

	public double getProbAcerto() {
		return probAcerto;
	}

	public void setProbAcerto(double probAcerto) {
		this.probAcerto = probAcerto;
	}

	public String getNomeMusical() {
		return nomeMusical;
	}

	public void setNomeMusical(String nomeMusical) {
		this.nomeMusical = nomeMusical;
	}

	public double[] getArResultado() {
		return arResultado;
	}

	public void setArResultado(double[] arResultado) {
		this.arResultado = arResultado;
	}

	public ArrayList<Double> getArResultEsperado() {
		return arResultEsperado;
	}

	public void setArResultEsperado(ArrayList<Double> arResultEsperado) {
		this.arResultEsperado = arResultEsperado;
	}

	public String getNomeNota() {
		return nomeNota;
	}

	public void setNomeNota(String nomeNota) {
		this.nomeNota = nomeNota;
	}
}
