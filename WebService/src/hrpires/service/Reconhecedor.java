package hrpires.service;

/**
* @author Heverton Pires da Luz
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import jm.gui.wave.WaveFileReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.MomentumBackpropagation;
import org.neuroph.util.TransferFunctionType;
import hrpires.service.FFT.FFT;

public class Reconhecedor {
	private static String dirPrincipal;
	private static String dirTeste;
	private static String dirRNA;
	private static JSONArray notasInfo;
	private static JSONObject objJson;
	private static JSONParser objJSONParser;
	private static final int TAXA_AMOSTRAGEM = 44100;
	private static final int INPUT_SIZE = 4097;
	private static final int OUTPUT_SIZE = 6;
	private static final int HIDDEN_SIZE = 8;
	private static final double ERROR = 0.01;
	private static final int TIME_SIZE = 8192;
	private static DataSet objDataSet;
	private static NeuralNetwork<?> neuralNetwork;
	private static String dirResultJson;

	public static void ini() {
		inicializar();
		if (neuralNetwork == null) {
			InputStream inputStream = getRede(dirRNA);
			neuralNetwork = NeuralNetwork.load(inputStream);
		}
	}

	public static String getDirTeste() {
		if (dirTeste == null)
			inicializar();
		return dirTeste;
	}

	public static void setDirTeste(String dirTeste) {
		Reconhecedor.dirTeste = dirTeste;
	}

	private static Retorno geraRetornoResult(double[] resultObtido) {
		JSONObject json;
		ArrayList<Double> resultEsperado = null;
		String nomeNota;
		String nomemusical;
		double calcQuadDifErro;
		double menorCalcQuadDifErro = Double.MAX_VALUE;
		Retorno objRetorno = new Retorno(resultObtido, null, "--", "--", 0);
		for (int i = 0; i < notasInfo.size(); i++) {
			json = (JSONObject) notasInfo.get(i);
			nomeNota = (String) json.get("nome");
			nomemusical = (String) json.get("nomeMusical");
			resultEsperado = getConjuntoTreinamento_Resultados(json);
			calcQuadDifErro = Estatistica.quadradoDiferenca(resultObtido, resultEsperado);
			if (calcQuadDifErro < menorCalcQuadDifErro) {
				menorCalcQuadDifErro = calcQuadDifErro;
				objRetorno.setRetorno(resultObtido, resultEsperado, nomeNota, nomemusical,
						Estatistica.calcPorcentAcerto(calcQuadDifErro));
			}
		}
		return objRetorno;
	}

	private static void gerarCSVResult(ArrayList<Retorno> arOutput) throws IOException {
		SimpleDateFormat dataFormatada = new SimpleDateFormat("HH-MM-SS");
		Retorno objRetorno;
		String nome = dataFormatada.format(new GregorianCalendar().getTime());
		FileWriter write = new FileWriter(
				new File(dirResultJson + "CSV" + nome + "-" + arOutput.get(0).getNomeNota() + ".csv"));
		write.write(
				"Nota, Obtido 1, Obtido 2, Obtido 3, Obtido 4, Obtido 5, Obtido 6, Esperado 1, Esperado 2, Esperado 3, Esperado 4, Esperado 5, Esperado 6, Erro, Acerto(%) \n");
		for (int i = 0; i < arOutput.size(); i++) {
			objRetorno = arOutput.get(i);
			write.write(objRetorno.getNomeNota() + "," + objRetorno.getArResultado()[0] + ","
					+ objRetorno.getArResultado()[1] + "," + objRetorno.getArResultado()[2] + ","
					+ objRetorno.getArResultado()[3] + "," + objRetorno.getArResultado()[4] + ","
					+ objRetorno.getArResultado()[5] + "," + objRetorno.getArResultEsperado().get(0) + ","
					+ objRetorno.getArResultEsperado().get(1) + "," + objRetorno.getArResultEsperado().get(2) + ","
					+ objRetorno.getArResultEsperado().get(3) + "," + objRetorno.getArResultEsperado().get(4) + ","
					+ objRetorno.getArResultEsperado().get(5) + ","
					+ Estatistica.quadradoDiferenca(objRetorno.getArResultado(), objRetorno.getArResultEsperado()) + ","
					+ objRetorno.getProbAcerto() + "\n");
		}
		write.close();
	}

	public static ArrayList<Retorno> reconhecerNotas(String nomeArq) throws IOException {
		ArrayList<Retorno> arOutput = new ArrayList<>();
		Retorno objRetorno = null;
		if (neuralNetwork == null) {
			ini();
		}
		ArrayList<double[]> arInputData = getAmostraTeste(nomeArq);
		for (int i = 0; i < arInputData.size(); i++) {
			neuralNetwork.setInput(arInputData.get(i));
			neuralNetwork.calculate();
			objRetorno = geraRetornoResult(neuralNetwork.getOutput());
			arOutput.add(objRetorno);
		}
		gerarCSVResult(arOutput);
		return arOutput;
	}

	public static void inicializar() {
		try {
			if (objDataSet == null)
				objDataSet = new DataSet(INPUT_SIZE, OUTPUT_SIZE);
			if (objJSONParser == null) {
				objJSONParser = new JSONParser();
				objJson = (JSONObject) objJSONParser
						.parse(new FileReader(System.getProperty("user.dir") + "\\Config.json"));
				dirPrincipal = (String) objJson.get("dir_principal");
				dirTeste = (String) objJson.get("dir_teste");
				notasInfo = (JSONArray) objJson.get("notas");
				dirRNA = (String) objJson.get("dir_rna");
				dirResultJson = (String) objJson.get("dir_resultJson");
			}

		} catch (FileNotFoundException ex) {
			System.out.println("Diretorio do arquivo config.json deve ser em: " + System.getProperty("user.dir"));
		} catch (IOException | ParseException ex) {
			System.out.println("Diretorio do arquivo config.json deve ser em: " + System.getProperty("user.dir"));
		}

	}

	private static ArrayList<Double> getConjuntoTreinamento_Resultados(JSONObject obj) {
		String[] result = obj.get("result").toString().split(" ");
		ArrayList<Double> arResult = new ArrayList<>();
		for (String result1 : result) {
			arResult.add(Double.parseDouble(result1));
		}
		return arResult;
	}

	public static ArrayList<Double> getConjuntoDados_Seg(String dir, float segundos) {
		FFT fft = new FFT(TIME_SIZE, TAXA_AMOSTRAGEM);
		WaveFileReader objWav = new WaveFileReader(dir);
		segundos = segundos * TAXA_AMOSTRAGEM;
		float dados[] = objWav.getSamples(TIME_SIZE, Math.round(segundos));
		fft.forward(dados);
		dados = fft.getSpectrum();
		ArrayList<Double> arDados = new ArrayList<>();
		for (int i = 0; i < INPUT_SIZE; i++) {
			arDados.add(Double.parseDouble(String.valueOf(dados[i])));
		}
		return arDados;
	}

	private static void addDataSetRow(ArrayList<Double> input, ArrayList<Double> output) {
		objDataSet.addRow(new DataSetRow(input, output));
	}

	private static void configDataSet() {
		JSONObject objInfoNotas;
		ArrayList<Double> arConjuntoTreinamento = null;
		ArrayList<Double> arConjuntoTreinamentoResult = null;
		String path;
		float seg;
		for (int i = 0; i < notasInfo.size(); i++) {
			seg = 0;
			objInfoNotas = (JSONObject) notasInfo.get(i);
			arConjuntoTreinamentoResult = getConjuntoTreinamento_Resultados(objInfoNotas);
			path = dirPrincipal + objInfoNotas.get("dir");
			WaveFileReader objWav = new WaveFileReader(path);
			int iterador = Math.round(objWav.getWaveSize() / (TAXA_AMOSTRAGEM / 5));
			for (int j = 0; j < iterador; j++) {
				arConjuntoTreinamento = getConjuntoDados_Seg(path, seg);
				addDataSetRow(arConjuntoTreinamento, arConjuntoTreinamentoResult);
				seg += 0.2;

			}

			System.out.println(iterador);
			System.out.println(path);
		}
	}

	@SuppressWarnings("unused")
	private static void start() {
		NeuralNetwork<?> neuralNetwork = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, INPUT_SIZE, HIDDEN_SIZE,
				OUTPUT_SIZE);
		configDataSet();
		MomentumBackpropagation sup = (MomentumBackpropagation) neuralNetwork.getLearningRule();
		sup.setMaxError(ERROR);
		neuralNetwork.learn(objDataSet);
		neuralNetwork.resumeLearning();
		neuralNetwork.save("RNA/music.nnet");
	}

	private static ArrayList<double[]> getAmostraTeste(String nomeArquivo) {
		FFT fft = new FFT(TIME_SIZE, TAXA_AMOSTRAGEM);
		WaveFileReader objWav = new WaveFileReader(nomeArquivo);
		int iterador = objWav.getWaveSize() / (TAXA_AMOSTRAGEM / 5);
		ArrayList<double[]> arDados = new ArrayList<>();
		float[] vetDadosFloat;
		double[] vetDadosDouble;
		float seg = 0;
		for (int i = 0; i < iterador; i++) {
			vetDadosDouble = new double[INPUT_SIZE];
			vetDadosFloat = objWav.getSamples(TIME_SIZE, Math.round(seg * TAXA_AMOSTRAGEM));
			fft.forward(vetDadosFloat);
			vetDadosFloat = fft.getSpectrum();
			for (int j = 0; j < INPUT_SIZE; j++) {
				vetDadosDouble[j] = Double.parseDouble(String.valueOf(vetDadosFloat[j]));
			}
			seg += 0.2;
			arDados.add(vetDadosDouble);
		}
		return arDados;
	}

	private static InputStream getRede(String path) {
		InputStream inputstream = null;
		try {
			inputstream = new FileInputStream(path);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return inputstream;
	}

}
