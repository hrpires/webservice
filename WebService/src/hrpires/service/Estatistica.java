package hrpires.service;

import java.util.ArrayList;

/**
 *
 * @author Joas Weslei Baia
 */
public class Estatistica {

	public static final double ACERTO_TOTAL = 2.45;
	
	public static double correlacaoR(double resultado[], ArrayList<Double> esperado) {
		double somaX = 0.0;
		double somaXX = 0.0;
		double somaXY = 0.0;
		double somaY = 0.0;
		double somaYY = 0.0;
		double numerador;
		double denominador;
		int n = resultado.length;
		for (int i = 0; i < n; i++) {
			somaX = somaX + resultado[i];
			somaY = somaY + esperado.get(i);
			somaXY = somaXY + (resultado[i] * esperado.get(i));
			somaXX = somaXX + (resultado[i] * resultado[i]);
			somaYY = somaYY + (esperado.get(i) * esperado.get(i));
		}
		numerador = somaXY - (somaX * somaY / n);
		denominador = Math.sqrt(((somaXX - (somaX * somaX / n))) * (somaYY - (somaY * somaY / n)));
		return numerador / denominador;
	}
	
	public static double quadradoDiferenca(double resultado[], ArrayList<Double> esperado) {
		ArrayList<Double> arResult = new ArrayList<>();
		double calc = 0;
		for(int i=0;i<esperado.size();i++) {
			calc = resultado[i]-esperado.get(i);
			calc = Math.pow(calc, 2);
			arResult.add(calc);
		}
		double result = 0;
		for(int i=0;i<arResult.size();i++) {
			result += arResult.get(i);	
		}
		return Math.sqrt(result);
	}
	
	public static double calcPorcentAcerto(double erro) {
		double erroPorc = erro * 100.0;
		double valorX = erroPorc/ACERTO_TOTAL;
		return 100 - valorX;
	}

}
